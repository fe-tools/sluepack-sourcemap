var SourceNode = require("source-map").SourceNode;
var SourceMapConsumer = require("source-map").SourceMapConsumer;

function Source() {}

module.exports = Source;

Source.prototype.source = null;

Source.prototype.size = function(options) {
	return this.source().length;
};

Source.prototype.map = function(options) {
	options = options || {};
    if(options.columns === false) {
        return this.listMap(options).toStringWithSourceMap().map;
    }

    return this.node(options).toStringWithSourceMap({file:"x"}).map.toJSON();
};

Source.prototype.sourceAndMap = function(options) {
	options = options || {};
    if(options.columns === false) {
        //console.log(this.listMap(options).debugInfo());
        return this.listMap(options).toStringWithSourceMap();
    }

    var res = this.node(options).toStringWithSourceMap({file:"x"});
    var _map = res.map.toJSON();
    return {
        source: res.code,
        map: res.map.toJSON()
    };
};

Source.prototype.node = null;

Source.prototype.listNode = null;

Source.prototype.updateHash = function(hash) {
	var source = this.source();
	hash.update(source || "");
};